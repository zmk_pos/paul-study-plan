package org.paul.blockChain;

import java.util.List;

/**
 * @author ：zmk
 * @date ：Created in 2021/12/3 9:52
 * @description：钱包
 * @modified By：
 * @version: V-
 */
public class Wallet {

    /**
     * 查询余额
     *
     * @param blockchain
     * @param address
     * @return
     */
    public static int getWalletBalance(List<Block> blockchain, String address) {
        int balance = 0;
        for (Block block : blockchain) {
            List<Transaction> transactions = block.getTransactions();
            for (Transaction transaction : transactions) {
                if (address.equals(transaction.getRecipient())) {
                    balance += transaction.getAmount();
                }
                if (address.equals(transaction.getSender())) {
                    balance -= transaction.getAmount();
                }
            }
        }
        return balance;
    }
}
