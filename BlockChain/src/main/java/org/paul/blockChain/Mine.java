package org.paul.blockChain;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @author ：zmk
 * @date ：Created in 2021/12/3 9:49
 * @description：挖矿(公式：Hash = SHA-256（区块链的最后一个区块的Hash + 需记账交易记录信息 + 随机数）)
 * @modified By：
 * @version: V-
 */
public class Mine {
    /**
     * 挖矿
     *
     * @param blockchain 整个区块链
     * @param txs        需记账交易记录
     * @param address    矿工钱包地址
     * @return
     */
    public static void mineBlock(List<Block> blockchain, List<Transaction> txs, String address) {
        //加入系统奖励的交易，默认挖矿奖励10个比特币
        Transaction sysTx = new Transaction(UUIDUtil.getUUID64(), "", address, 10);
        txs.add(sysTx);
        //获取当前区块链里的最后一个区块
        Block latestBlock = blockchain.get(blockchain.size() - 1);
        //随机数
        int nonce = 1;
        String hash = "";
        while (true) {
            hash = CryptoUtil.SHA256(latestBlock.getHash() + JSON.toJSONString(txs) + nonce);
            if (hash.startsWith("0000")) {
                System.out.println("=====计算结果正确，计算次数为：" + nonce + ",hash:" + hash);
                break;
            }
            nonce++;
            System.out.println("计算错误，hash:" + hash);
        }

        //解出难题，可以构造新区块并加入进区块链里
        Block newBlock = new Block(latestBlock.getIndex() + 1, System.currentTimeMillis(), txs, nonce, latestBlock.getHash(), hash);
        blockchain.add(newBlock);
        System.out.println("挖矿后的区块链：" + JSON.toJSONString(blockchain));
    }
}
