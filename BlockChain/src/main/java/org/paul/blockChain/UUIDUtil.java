package org.paul.blockChain;

import java.util.UUID;

/**
 * @author ：zmk
 * @date ：Created in 2021/12/3 10:01
 * @description：
 * @modified By：
 * @version: V-
 */
public class UUIDUtil {
    //获取32长度的UUID字符串
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    //获取64长度的UUID字符串
    public static String getUUID64() {
        return getUUID() + getUUID();
    }

}
