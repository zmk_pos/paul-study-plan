package org.paul.seckillDemo.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/24 11:07
 * @description
 */
@Component
public class InitStockData implements ApplicationRunner {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        stringRedisTemplate.opsForValue().set(String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50"), "50");

  /*      for (int i = 0; i < 5; i++) {
            stringRedisTemplate.opsForValue().set(String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", i), "10");
        }*/

        for (int i = 0; i < 5; i++) {
            stringRedisTemplate.opsForValue().set(String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_CLUSTER, "AX01", 50, i), "10");
        }
    }
}
