package org.paul.seckillDemo.common;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/24 11:09
 * @description
 */
public class RedisCatalogConst {
    public static final String PRODUCT_CODE_STOCK_LOCK="product:%s:%s:lock";
    public static final String PRODUCT_CODE_STOCK="product:%s:%s";
    public static final String PRODUCT_CODE_STOCK_CLUSTER="product:%s:%s:%s";
}
