package org.paul.seckillDemo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.paul.seckillDemo.common.RedisCatalogConst;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/23 14:27
 * @description 秒杀架构演变阶段【来自图灵课堂听课】
 */
@Api(description = "秒杀架构演变阶段")
@RequestMapping("/api/seckill")
@RestController
public class SeckillController {

    @Autowired
    private Redisson redisson;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @ApiOperation(value = "库存扣减【单机版】")
    @GetMapping("/deduct_stock/single/v1")
    public String deductStock() throws InterruptedException {
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");
        int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
        if (stock > 0) {
            int realStock = stock - 1;
            stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
            System.out.println("扣减成功，剩余库存：" + realStock);
        } else {
            System.out.println("扣减失败，库存不足");
        }
        return "end";
    }

    @ApiOperation(value = "库存扣减【单机版】")
    @GetMapping("/deduct_stock/single/v2")
    public String deductStock2() throws InterruptedException {
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");
        int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
        synchronized (this) {
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + realStock);
            } else {
                System.out.println("扣减失败，库存不足");
            }
        }
        return "end";
    }

    /**
     * 一般集群下处理模式
     *
     * @return
     * @throws InterruptedException
     */
    @ApiOperation(value = "库存扣减【集群版】")
    @GetMapping("/deduct_stock/cluster/v1")
    public String deductStockC1() throws InterruptedException {
        String lockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_LOCK, "AX01", "50");
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");

        Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(lockKey, "1");
        //redis宕机 ，v2解决
        stringRedisTemplate.expire(lockKey, 60, TimeUnit.SECONDS);
        if (!result) {
            return "error";
        }
        try {
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + realStock);
            } else {
                System.out.println("扣减失败，库存不足");
            }
        } finally {
            stringRedisTemplate.delete(lockKey);
        }

        return "end";
    }

    /**
     * 高并发下，存在锁频繁删除，无效情况（v3解决）
     *
     * @return
     * @throws InterruptedException
     */
    @ApiOperation(value = "库存扣减【集群版】")
    @GetMapping("/deduct_stock/cluster/v2")
    public String deductStockC2() throws InterruptedException {
        String lockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_LOCK, "AX01", "50");
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");

        Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(lockKey, "1", 10, TimeUnit.SECONDS);
        if (!result) {
            return "error";
        }
        try {
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + realStock);
            } else {
                System.out.println("扣减失败，库存不足");
            }
        } finally {
            stringRedisTemplate.delete(lockKey);
        }

        return "end";
    }

    /**
     * 问题：业务执行时间过长，存在锁自动过期问题 （v4解决）
     *
     * @return
     * @throws InterruptedException
     */
    @ApiOperation(value = "库存扣减【集群版】")
    @GetMapping("/deduct_stock/cluster/v3")
    public String deductStockC3() throws InterruptedException {
        String lockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_LOCK, "AX01", "50");
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");

        String clientId = UUID.randomUUID().toString();
        try {
            Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(lockKey, "1", 10, TimeUnit.SECONDS);
            if (!result) {
                return "系统繁忙，请稍后再试";
            }
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + realStock);
            } else {
                System.out.println("扣减失败，库存不足");
            }
        } finally {
            if (clientId.equals(stringRedisTemplate.opsForValue().get(lockKey))) {
                stringRedisTemplate.delete(lockKey);
            }
        }

        return "end";
    }

    /**
     * 存在扣减库存性能问题（v5解决）
     *
     * @return
     * @throws InterruptedException
     */
    @ApiOperation(value = "库存扣减【集群版】")
    @GetMapping("/deduct_stock/cluster/v4")
    public String deductStockC4() throws InterruptedException {
        String lockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_LOCK, "AX01", "50");
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK, "AX01", "50");

        //自带while循环，存在性能问题
        RLock redissonLock = redisson.getLock(lockKey);
        try {
            redissonLock.lock(10, TimeUnit.SECONDS);

            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + realStock);
            } else {
                System.out.println("扣减失败，库存不足");
            }
        } finally {
            redissonLock.unlock();
        }

        return "end";
    }

    /**
     * 存在扣减库存性能问题（v5解决）
     * 库存分段存储：标识_商品编码_序号(product_0001_0001
     * 锁：标识_商品编码_序号(product_0001_0001
     * 根据取模来进行加锁及扣减库存
     *
     * @return
     * @throws InterruptedException
     */
    @ApiOperation(value = "库存扣减【集群版】")
    @GetMapping("/deduct_stock/cluster/v5")
    public String deductStockC5(@RequestParam Integer userId) throws InterruptedException {
//        int userId = 100612;

        //处理分段
        Integer val = userId % 10;
        String lockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_LOCK, "AX01", "50") + "_" + String.format("%03d", val);
        String stockKey = String.format(RedisCatalogConst.PRODUCT_CODE_STOCK_CLUSTER, "AX01", "50", val);

        //
        RLock redissonLock = redisson.getLock(lockKey);
        try {
            redissonLock.lock(60, TimeUnit.SECONDS);

            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get(stockKey));
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set(stockKey, realStock + "");
                System.out.println("扣减成功，剩余库存：" + userId + "|" + realStock + "|" + stockKey);
            } else {
                System.out.println("扣减失败，库存不足：" + userId + "|" + stockKey);
            }

        } catch (Exception ex) {

        } finally {
            if (redissonLock.isLocked())
                redissonLock.unlock();
        }

        return "end";
    }

}
