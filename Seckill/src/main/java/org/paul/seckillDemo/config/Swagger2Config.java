package org.paul.seckillDemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/1/11 9:29
 * @description
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket swaggerPersonApi10() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("default")
                .globalOperationParameters(setToken())
                .apiInfo(new ApiInfoBuilder().version("default").title("Cspt Admin Swagger API").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage(Swagger2Config.class.getName().replace("config.Swagger2Config", "controller")))
                .paths(regex("/api/.*"))
                .build();
    }

    @Bean
    public Docket swaggerPersonApi20() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v1")
                .globalOperationParameters(setToken())
                .apiInfo(new ApiInfoBuilder().version("1.0").title("Cspt Admin Swagger API").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage(Swagger2Config.class.getName().replace("config.Swagger2Config", "controller")))
                .paths(regex("/v1/api/.*"))
                .build();
    }


    /**
     * 设置token参数
     */
    private List<Parameter> setToken() {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        tokenPar.name("Authorization").defaultValue("Bearer ").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        return pars;
    }
}
