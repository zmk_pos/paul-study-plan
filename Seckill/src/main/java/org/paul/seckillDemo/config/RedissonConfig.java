package org.paul.seckillDemo.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/24 11:37
 * @description
 */
@Component
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String REDIS_HOST;
    @Value("${spring.redis.port}")
    private String REDIS_PORT;
    @Value("${spring.redis.password}")
    private String REDIS_PASSWORD;
    @Value("${spring.redis.database}")
    private Integer REDIS_DATABASE;

    @Bean
    public Redisson redisson() {
        //此为单机模式
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + REDIS_HOST + ":" + REDIS_PORT).setPassword(REDIS_PASSWORD).setDatabase(REDIS_DATABASE);
        return (Redisson) Redisson.create(config);
    }
}
