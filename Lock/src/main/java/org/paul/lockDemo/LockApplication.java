package org.paul.lockDemo;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/26 14:55
 * @description
 *
 * ###原子性问题：lock cmpxchgq 缓存行锁/总线锁
 * 缓存行锁>64字节，则为总线锁
 *
 * ###ABA问题：多个线程同时修改值
 * AtomicInteger》AtomicStampedReference（加版本号）
 *
 * ###CAS 自旋锁 ，高并发下会导致耗CPU
 * 引入消息队列
 *
 * ###轻量级锁 升级 重量级锁
 * 多个线程运行  AtomicInteger 锁升级重量级
 *
 * ###案例：i++
 * synchronized》AtomicInteger》AtomicStampedReference》LongAdder(分段CAS)
 *
 */
public class LockApplication {
    public static void main(String[] args) throws InterruptedException {
        E a = new E();
//        AtomicStampedReference<Integer> stampedReference = new AtomicStampedReference(0, 1);
        Long start = System.currentTimeMillis();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000000; i++) {
                a.increase();
            }
        });

        t1.start();

        for (int i = 0; i < 10000000; i++) {
            a.increase();
        }

        t1.join();

        Long end = System.currentTimeMillis();
        System.out.println(String.format("%sms：", end - start));
        System.out.println(a.getNum());
    }
}
