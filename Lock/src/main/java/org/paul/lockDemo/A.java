package org.paul.lockDemo;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/26 15:02
 * @description
 */
public class A {

    private int num = 0;

    public long getNum() {
        return num;
    }

    public void increase() {
        num++;
    }
}
