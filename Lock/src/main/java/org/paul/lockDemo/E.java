package org.paul.lockDemo;

import java.util.concurrent.atomic.LongAdder;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/26 15:43
 * @description
 */
public class E {
    private int num = 0;

    LongAdder longAdder = new LongAdder();

    public long getNum() {
        return longAdder.longValue();
    }

    // 无锁，自旋锁，乐观锁，轻量级锁（compareAndSet ，compareAndSwap）
    public void increase() {
        longAdder.increment();
    }
}
