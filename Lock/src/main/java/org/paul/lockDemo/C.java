package org.paul.lockDemo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/26 15:16
 * @description 存在ABA问题
 */
public class C {

    private int num = 0;

    AtomicInteger atomicInteger = new AtomicInteger();

    public long getNum() {
        return atomicInteger.get();
    }

    // 无锁，自旋锁，乐观锁，轻量级锁（compareAndSet ，compareAndSwap）
    public void increase() {
        atomicInteger.decrementAndGet();

//        int oldValue = atomicInteger.get();
//        int newValue = oldValue + 1;
//        atomicInteger.compareAndSet(oldValue, newValue); //CAS
    }
}
