package org.paul.lockDemo;

/**
 * @author zmk
 * @version 1.0
 * @date 2021/3/26 15:16
 * @description
 */
public class B {
    private int num = 0;

    public long getNum() {
        return num;
    }

    //互斥锁，悲观锁，同步锁，重量级锁（线程阻塞，上下文切换，操作系统线程调度）
    public synchronized void increase() {
        num++;
//        synchronized (this) {
//            num++;
//        }
    }
}
