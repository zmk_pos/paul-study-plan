package org.paul.log.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：zmk
 * @date ：Created in 2021/12/10 11:03
 * @description：
 * @modified By：
 * @version: V-
 */
@CrossOrigin
@RestController
@RequestMapping("/test")
public class TestController {

    private final static Logger logger = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/info")
    public void info() {
        for (int i = 0; i < 10; i++) {
            logger.info("测试" + i);
            logger.info("测试.........." + i);
            logger.info("你好.........." + i);
        }

    }


}
