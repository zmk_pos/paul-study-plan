package org.paul.log.web.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author ：zmk
 * @date ：Created in 2021/12/10 15:40
 * @description：
 * @modified By：
 * @version: V-
 */
//@Component
public class KafkaConsume {

    /**
     * kafka 日志监听
     *
     * @param record
     */
    @KafkaListener(topics = "kafka-Log-Test")
    public void listen(ConsumerRecord<?, ?> record) {
        System.out.printf(record.topic()+"----"+record.value()+"\n");
    }
}
