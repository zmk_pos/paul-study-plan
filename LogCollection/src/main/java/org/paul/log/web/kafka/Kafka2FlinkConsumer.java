package org.paul.log.web.kafka;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

import java.util.Properties;


/**
 * @author ：zmk
 * @date ：Created in 2021/12/10 15:47
 * @description：
 * @modified By：
 * @version: V-
 */
public class Kafka2FlinkConsumer {
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //输入kafka信息
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "192.168.30.65:9092");
        properties.setProperty("group.id", "kafka-log-test-consumer");
        //读取数据,第一个参数是kafka的topic，也就是上面filebeat配置文件里面设定的topic叫log
        FlinkKafkaConsumer<String> myConsumer = new FlinkKafkaConsumer<>("kafka-Log-Test", new SimpleStringSchema(), properties);
        //设置只读取最新数据
        myConsumer.setStartFromEarliest();
        DataStream<String> stream = env.addSource(myConsumer);
        env.enableCheckpointing(5000);
        stream.print();
        env.execute("flink consumer kafka infos");
    }

}
