package org.paul.log.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:14
 * @description：
 * @modified By：
 * @version: V-
 */
@SpringBootApplication
public class LogApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(LogApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LogApplication.class);
    }
}
